// Proxy Example

const obj = { text: 'Hello', count: 0 };
const customObj = new Proxy(obj, {
  get: (obj, property) => {
    console.log(obj[property]);
    return obj[property];
  }
});

const textValue = customObj.text;
const countValue = customObj.count;

// Reflect Example

const obj2 = {
  text: 'Hello my friend'
};

function AddString(obj) {
  return new Proxy(obj, {
    get: (obj, property) => {
      if (property.endsWith('Upper')) {
        const newProp = property.replace('Upper', '');
        return obj[newProp].toUpperCase();
      }
      if (property.endsWith('Lower')) {
        const newProp = property.replace('Lower', '');
        return obj[newProp].toLowerCase();
      }

      return Reflect.get(obj, property);
    }
  });
}

const customObj2 = AddString(obj2);
console.log(customObj2.text);
console.log(customObj2.textUpper);
console.log(customObj2.textLower);
