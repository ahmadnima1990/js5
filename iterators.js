const numbers = [4, 2, 3];
const numbersIterator = numbers[Symbol.iterator]();

console.log(numbersIterator.next());
console.log(numbersIterator.next());
console.log(numbersIterator.next());
console.log(numbersIterator.next());

for (const n of numbers) {
  console.log(n);
}

//--------------------------//
//--------------------------//
//--------------------------//

function squared(max) {
  return {
    [Symbol.iterator]() {
      let n = 0;

      return {
        next() {
          n++;
          if (n <= max) {
            return {
              value: n * n,
              done: false
            };
          }

          return {
            value: undefined,
            done: true
          };
        }
      };
    }
  };
}

for (const n of squared(10)) {
  console.log(n);
}
